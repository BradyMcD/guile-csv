TARGET := $(shell guile -c "(display (%site-dir))")
CCACHE := $(shell guile -c "(display(%site-ccache-dir))")
OBJ := csv.go

.PHONY: all clean install uninstall
all: $(OBJ)

%.go: csv/%.scm
	@GUILE_AUTO_COMPILE=0 guild compile $< -o $@

install: $(TARGET)/csv $(CCACHE)/csv

$(TARGET)/csv:
	@echo "Installing source..."
	cp    -fr csv/   $(TARGET)
	@echo "Install complete."

$(CCACHE)/csv:
	@echo "Installing objects to cache..."
	mkdir -p         $(CCACHE)/csv/
	cp    -fr *.go   $(CCACHE)/csv/
	@echo "Cache complete."

uninstall:
	rm -fr $(TARGET)/csv
	rm -fr $(CCACHE)/csv

clean:
	rm -f $(OBJ)
