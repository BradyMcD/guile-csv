guile-csv
=========

A csv reader for Guile. Forked from [Nala Ginrut's][og-repo] repository of the 
same name. I forked simply because I plan on using this code (or some 
derivative) and as my needs change so too will the interface. Pushing breaking 
changes to a 5 year old repository isn't exactly my idea of a good time.

### install

`$make`

`$sudo make install`

## General usage

### reading

First get yourself a port out of whatever csv you want to process:

```scheme
(use-modules (csv csv))
(define $port (open-input-string "a,b\n1,2\n3,4"))
```

Then, you can process it into a form of your choice between xml and sxml.

```scheme
(csv->xml $port)
```

Output:
```
"<record-1><a>1</a><b>2</b></record-1><record-2><a>3</a><b>4</b></record-2>"
```

If you plan on working with the parsed output further in scheme you'll probably
want to use `(csv->sxml)` with the optional argument `#:record-sym` like 
so:

```scheme
(csv->sxml $port
           #:record-sym (lambda n "rec"))
```

Output:
```sheme
(*TOP* (rec ((a "1") (b "2"))) (rec ((a "3") (b "4"))))
```

`#:record-sym` expects a procedure and is called with a numeric argument to
generate a string. Ignoring the argument and using a single symbol for all 
records as seen in the example above could be useful if you plan on 
manipulating the resulting tree using something like `(sxml transform)`.

### writing

While there are facilities to write csv strings and files provided user be 
warned: they don't quite do what they say on the tin. Calling `sxml->csv` 
with the output from `csv->sxml` for example will result in an error.  
The "sxml" expected by this function is what I would venture to call "scsv".  
```scheme
`((a b c)("1" "2" "3")("4" "5" "6"))
```  
Note the double quotes surrounding each data point, the algorithm currently
does not attempt to make any conversions and `(string-join)` will complain
accordingly.

I don't have any need for converting back into csv and have no personal plans 
on improving this situation (just in case you were preparing to hold your 
breath). If anyone reading this does want such functionality and has a 
description of how they think the function ought to act please do raise an 
issue.

[og-repo]:https://gitlab.com/NalaGinrut/guile-csv
